const doctrine = require('doctrine')
const env = require('jsdoc/env')

/**
 * @constant {string} JSDOC_MERMAID_TAG
 * @description Matches starting strings of a jsdoc comment
 */
const JSDOC_MERMAID_TAG = /@mermaid\b/

/**
 * @constant {Object} ESCAPE_HTML_MAPPING
 * @description Map of all characters that need HTML escaping
 */
const ESCAPE_HTML_MAPPING = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;'
}

/**
 * Escapes all special char in HTML
 * @param {string} str - A char
 * @returns {string} An escaped char
 */
function escapeHtml (str) {
  return str.replace(/[&<>"']/g, c => ESCAPE_HTML_MAPPING[c])
}

/**
 * Checks if a doclet is a mermaid one
 * @param {string} docletComment - A doclet comment
 * @returns {boolean} Returns true if it is a mermaid tag
 */
function isStartingMermaidTag (docletComment) {
  return docletComment && JSDOC_MERMAID_TAG.test(docletComment)
}

const isAddedMermaid = {}

/**
 * Checks if the mermaid script tags should be generated
 * @param {string} docletMemberOf - The parent module of the doclet
 * @param {boolean} disableScript - Configured flag that disables all scripts
 * @returns {boolean} Returns true if the script should be generated
 */
function shouldGenerateMermaidScript (docletMemberOf, disableScript) {
  return !isAddedMermaid[docletMemberOf] && !disableScript
}

/**
 * Flags that the current module has generated scripts.
 * It prevents to load multiple JavaScript modules on the same page.
 * @param {string} docletMemberOf - The parent module of the doclet
 */
function registerGeneratedScript (docletMemberOf) {
  isAddedMermaid[docletMemberOf] = true
}

/**
 * @constant {string} MERMAID_DEFAULT_VERSION
 * @description Semver Mermaid version
 */
const MERMAID_DEFAULT_VERSION = '8.4.8'

/**
 * Gets the plugin configuration by overriding the default values
 * @param {string} [version=8.4.8] - Set the used version of mermaid
 * @param {string} style - Additional CSS style on the the mermaid diagram
 * @param {boolean} disableScript - Shut down all generated javascript scripts
 * @param {object} initConfig - Custom configuration of mermaid
 * @returns {object} Configuration object of the plugin
 */
function getPluginConfig ({ version, style, disableScript, initConfig } = {}) {
  return {
    version: '@' + (version || MERMAID_DEFAULT_VERSION),
    style: style || '',
    disableScript: disableScript || false,
    initConfig: initConfig || {}
  }
}

/**
 * Gets the HTML script tag that loads mermaid
 * @param {string} version - Version of mermaid
 * @returns {string} HTML script tag that loads mermaid
 */
function getMermaidLoadScript (version) {
  return `<script src="https://unpkg.com/mermaid${version}/dist/mermaid.min.js"></script>`
}

/**
 * Gets the HTML script tag that initializes mermaid
 * @param {object} initConfig - Mermaid configuration
 * @returns {string} HTML script tag that initializes mermaid
 */
function getMermaidInitScript (initConfig) {
  return `<script>mermaid.initialize(${JSON.stringify(initConfig)});</script>\n`
}

/**
 * Gets all HTML scripts that setups mermaid
 * @param {string} version - Version of mermaid
 * @param {object} initConfig - Configuration of mermaid
 * @returns {string} HTML scripts for mermaid
 */
function getMermaidScript (version, initConfig) {
  return getMermaidLoadScript(version) + getMermaidInitScript(initConfig)
}

/**
 * Finds all doclet tags that represent mermaid diagrams
 * @param {string} docletComment - The doclet comment
 * @returns {object[]} An array of doclet tags
 */
function findMermaidDocletTags (docletComment) {
  return doctrine.parse(docletComment, {
    unwrap: true,
    tags: ['mermaid'],
    recoverable: true
  }).tags
}

/**
 * Gets the HTML div of the mermaid diagram
 * @param {string} configStyle - Configured style
 * @param {string} tagDescription - Description of the doclet tag
 * @returns {string} HTML div of the mermaid script
 */
function getMermaidHtmlDiv (configStyle, tagDescription) {
  return `
      <div class="mermaid" style="${this.style}">
        ${escapeHtml(tagDescription)}
      </div>
    `
}

const { version, style, disableScript, initConfig } = getPluginConfig(env.conf.mermaid)

exports.handlers = {
  defineTags: function (dictionary) {
    dictionary.defineTag('mermaid')
  },
  newDoclet: function (e) {
    if (isStartingMermaidTag(e.doclet.comment)) {
      const mermaidTags = findMermaidDocletTags(e.doclet.comment)
      const mermaidHtmls = mermaidTags.map(tag => getMermaidHtmlDiv(style, tag.description))

      if (mermaidHtmls) {
        e.doclet.description = e.doclet.description || ''

        if (shouldGenerateMermaidScript(e.doclet.memberof)) {
          e.doclet.description += getMermaidScript(version, initConfig)
          registerGeneratedScript(e.doclet.memberof)
        }

        e.doclet.description += mermaidHtmls.join('')
      }
    }
  }
}
